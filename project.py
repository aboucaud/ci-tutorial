# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np

def buffon_pi(n_total=100000):
    """Pi value obtained with Buffon method"""
    np.random.seed(0)
    x = np.random.uniform(-1, 1, n_total)
    y = np.random.uniform(-1, 1, n_total)

    n_inside = np.sum(x**2 + y**2 < 1)
    pi = 4 * n_inside / n_total

    return pi


def wallis_pi(n_total=100000):
    """Pi value computed with Wallis method"""
    n = np.arange(1, n_total)
    fournsq = 4 * n ** 2
    halfpi = np.product(fournsq / (fournsq - 1))

    return 2 * halfpi
