from __future__ import division

import pytest
import numpy as np

from project import buffon_pi, wallis_pi

class TestPi(object):
    n_total = 1e6

    def test_buffon(self):
        pi = buffon_pi(n_total=self.n_total)
        assert np.abs(np.pi - pi) / np.pi < 1e-4

    def test_wallis(self):
        pi = wallis_pi(n_total=self.n_total)
        assert np.abs(np.pi - pi) / np.pi < 1e-4
