# GitLab Continuous Integration tutorial project
[![build status](https://git.ias.u-psud.fr/aboucaud/ci-tutorial/badges/master/build.svg)](https://git.ias.u-psud.fr/aboucaud/ci-tutorial/commits/master)
[![coverage report](https://git.ias.u-psud.fr/aboucaud/ci-tutorial/badges/master/coverage.svg)](https://git.ias.u-psud.fr/aboucaud/ci-tutorial/commits/master)


This project serves as material for a [course on continuous integration with GitLab](http://www.ias.u-psud.fr/aboucaud/gitlab-ci.html).

## Project content

* Code to be tested => `project.py`
* Test suite for the code => `test_project.py`
* Continuous integration config file => `.gitlab-ci.yml`

